/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

/// <reference path="./types/global.d.ts"/>
import { Core } from '../src/core';
import { expect } from 'chai';
import 'mocha';

describe('Simple Cmd run', () => {
    it('should return {"script":[{"cmd":["CHGLIBL LIBL(DB2JSON QTEMP) CURLIB(DB2JSON)"]}]}', (done: () => void) => {
        const core = new Core(global.db2Url);

        let cmd = core.command('CHGLIBL LIBL(DB2JSON QTEMP) CURLIB(DB2JSON)');
        core.add(cmd);
        
        let result = core.run((result) => {
            expect(JSON.stringify(result)).to.equal('{"script":[{"cmd":["CHGLIBL LIBL(DB2JSON QTEMP) CURLIB(DB2JSON)"]}]}');
            done();
        });
    });
});

describe('Simple Rexx run', () => {
    it('should return [{"CCSID":"37"},{"USRLIBL":"QGPL       QTEMP"},{"SYSLIBL":"QSYS       QSYS2      QHLPSYS    QUSRSYS"}]', (done: () => void) => {
        const core = new Core(global.db2Url);

        let cmd = core.command('RTVJOBA CCSID(?N) USRLIBL(?) SYSLIBL(?)');
        core.add(cmd);

        let result = core.run((result: any) => {
            let records = result['script'][0]['cmd'][1]['records'];
            expect(JSON.stringify(records)).to.equal('[{"CCSID":"37"},{"USRLIBL":"QGPL       QTEMP"},{"SYSLIBL":"QSYS       QSYS2      QHLPSYS    QUSRSYS"}]');
            done();
        })
    });
});

describe('Advanced multi-command run', () => {
    it('should return ', (done: () => void) => {
        const core = new Core(global.db2Url);

        let cmdChgLibl = core.command('CHGLIBL LIBL(DB2JSON QTEMP) CURLIB(DB2JSON)');
        let cmdGetLibl = core.command('RTVJOBA USRLIBL(?)')
        core.add(cmdChgLibl);
        core.add(cmdGetLibl);

        let result = core.run((result: any) => {
            let chgRecord = result['script'][0]['cmd'][0];
            let getRecord = result['script'][1]['cmd'][1]['records'];
            expect(JSON.stringify(chgRecord)).to.equal('"CHGLIBL LIBL(DB2JSON QTEMP) CURLIB(DB2JSON)"');
            expect(JSON.stringify(getRecord)).to.equal('[{"USRLIBL":"DB2JSON    QTEMP"}]');
            done();
        })
    });
})