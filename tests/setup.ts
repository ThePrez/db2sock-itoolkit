/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

/// <reference path="./types/global.d.ts"/>
import { Args } from './args';
import { expect } from 'chai';

// Retrieve the "--db2sock" argument value, and verify it is not empty. 
// Store it in the global namespace for use throughout tests
before(() => {
    let db2Url = Args.getArgument('--db2sock');
    if (db2Url.length === 0) {
        console.error('ERROR: Missing --db2sock argument');
    }
    expect(db2Url.length).gt(0);

    global.db2Url = db2Url;
});