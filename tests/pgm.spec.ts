/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

/// <reference path="./types/global.d.ts"/>
import { Core } from '../src/core';
import { expect } from 'chai';
import 'mocha';

describe('Pgm run', () => {
    it('should return {"char":"Hello World"}', (done: () => void) => {
        const core = new Core(global.db2Url);

        let pgm = core.program('HELLO', 'DB2JSON');
        pgm.addParam('char', '128a', 'Hi there');

        core.add(pgm);
        let result = core.run((result: any) => {
            let data = result['script'][0]['pgm'][2];
            expect(JSON.stringify(data)).to.equal('{"char":"Hello World"}');
            done();
        });
    });
});

describe('Pgm run - Multiple params', () => {
    it('should return ', (done: () => void) => {
        const core = new Core(global.db2Url);

        let pgm = core.program('HAMELA01', 'DB2JSON');
        let param1 = pgm.addParam('rows', '5s0', 20);
        let param2 = pgm.addParam('items');
        param2.setDim(20);
        param2.addField('field1', '5a', 'ff1');
        param2.addField('field2', '5a', 'ff2');
        param2.addField('field3', '5a', '');
        param2.addField('field4', '5a', '');
        let param3 = pgm.addParam('last', '10a', '');

        core.add(pgm);
        let result = core.run((result: any) => {
            expect(JSON.stringify(result)).to.contain('"rows":20');
            expect(JSON.stringify(result)).to.contain('"field1":"a1"');
            expect(JSON.stringify(result)).to.contain('"field2":"b17"');
            expect(JSON.stringify(result)).to.contain('"field4":"d20"');
            expect(JSON.stringify(result)).to.contain('"last":"TEST"');
            done();
        });
    });
});

describe('Pgm run - Multiple params in/out', () => {
    it('should return data structure', (done: () => void) => {
        const core = new Core(global.db2Url);

        let pgm = core.program('HAMELA02', 'DB2JSON');
        let param1 = pgm.addParam('inCount', '10i0', 5);
        let param2 = pgm.addParam('inputDS');
        param2.setDim(20);
        param2.addField('in1', '5av2', 'i1');
        param2.addField('in2', '5av2', 'i2');
        let param3 = pgm.addParam('outCount', '10i0', 5);
        let param4 = pgm.addParam('inputDS');
        param4.setDim(20);
        param4.addField('out1', '5av2', 'o1');
        param4.addField('out2', '5av2', 'o2');
        param4.addField('out3', '10av2', 'o3');
        let param5 = pgm.addParam('last', '10a', 'll');

        core.add(pgm);
        let result = core.run((result: any) => {
            expect(JSON.stringify(result)).to.contain('"inCount":5');
            expect(JSON.stringify(result)).to.contain('"outCount":5');
            expect(JSON.stringify(result)).to.contain('"out1":"i1"');
            expect(JSON.stringify(result)).to.contain('"out2":"i2"');
            expect(JSON.stringify(result)).to.contain('"out3":"i1i2"');
            expect(JSON.stringify(result)).to.contain('"out1":"o1"');
            expect(JSON.stringify(result)).to.contain('"out2":"o2"');
            expect(JSON.stringify(result)).to.contain('"out3":"o3"');
            expect(JSON.stringify(result)).to.contain('"last":"TEST"');
            done();
        });
    });
});
