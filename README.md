# db2sock-itoolkit - JS wrapper over DB2Sock's REST interface

## Table of Contents
- [Usage](#markdown-header-usage)
    - [CMD Call](#markdown-header-cmd-call)
    - [QSH Call](#markdown-header-qsh-call)
    - [PGM Call](#markdown-header-pgm-call)
    - [Advanced CMD Call](#markdown-header-advanced-cmd-call)
- [Building](#markdown-header-building)
    - [Available NPM Commands](#markdown-header-available-npm-commands)
- [Testing](#markdown-header-testing)
- [License](#markdown-header-license)

## Usage
The underlying concept for the db2sock-itoolkit is to make the interface as easy 
to use as possible for end users. This means that very few functions should take 
in plain JavaScript objects, and in the future should return formatted objects 
back to the user as opposed to just dumping the db2sock response on them.

### CMD Call
```javascript
    // Setup the toolkit core
    var tk = require("../db2sock-itoolkit");
    var core = new tk.Core('http://<IP>/db2json.db2');

    // Create a command call, and add it to the core
    var cmd = core.command('CHGLIBL LIBL(DB2JSON QTEMP) CURLIB(DB2JSON)');
    core.add(cmd);

    // Run the core's current call list, and get the results
    var result = core.run(function (result) {
        console.log('Results:');
        console.log(result);
    });
```

### QSH Call
```javascript
    // Setup the toolkit core
    var tk = require("../db2sock-itoolkit");
    var core = new tk.Core('http://<IP>/db2json.db2');

    // Create a shell call, and add it to the core
    var sh = core.shell('ls -1 /QOpenSys');
    core.add(sh);

    // Run the core's current call list, and get the results
    var result = core.run(function (result) {
        console.log('Results:');
        console.log(result);
    });
```

### PGM Call
```javascript
    // Setup the toolkit core
    var tk = require("../db2sock-itoolkit");
    var core = new tk.Core('http://<IP>/db2json.db2');

    // Create a program call with parameters, and add it to the core
    var pgm = core.program('HELLO', 'DB2JSON');
    pgm.addParam('char', '128a', 'Hi there');
    core.add(pgm);

    // Run the core's current call list, and get the results
    var result = core.run(function (result) {
        console.log('Results:');
        console.log(result);
    });
```

### Advanced CMD Call
It is possible to chain multiple calls under the same "session", this allows 
things like changing the library list prior to calling a command or program.
```javascript
    // Setup the toolkit core
    var tk = require("../db2sock-itoolkit");
    var core = new tk.Core('http://<IP>/db2json.db2');

    // Create multiple command calls, and add them to the core
    var cmdChgLibl = core.command('CHGLIBL LIBL(DB2JSON QTEMP) CURLIB(DB2JSON)');
    var cmdGetLibl = core.command('RTVJOBA USRLIBL(?)');
    core.add(cmdChgLibl);
    core.add(cmdGetLibl);

    // Run the core's current call list, and get the results
    var result = core.run(function (result) {
        console.log('Results:');
        console.log(result);
    });
```

## Building
This module has been built and tested with NodeJS v8.9. Compatibility with 
earlier versions of NodeJS is not guaranteed.

To set up the development environment, run `npm install` to install the required 
Node modules. The below commands will not work until the Node modules are 
installed.

### Available NPM Commands
- `npm run build` &mdash; Generate the JavaScript files
- `npm run clean` &mdash; Clean any previously built JavaScript files
- `npm run rebuild` &mdash; Clean and rebuild the JavaScript files
- `npm run test -- --db2sock=<url>` &mdash; Run the current test suite against 
the built JavaScript files. See [Testing](#markdown-header-testing) for more information.

## Testing
Most aspects of the db2sock-itoolkit should have tests written to cover their 
usage. Tests can be run with the `npm run test -- --db2sock=<url>` command, 
where `<url>` is the full URL to the DB2Sock CGI program. Note that the `--` 
after "test" is required for the test suite to get the next parameter value.

Certain tests will expect specific programs to exist, these programs must exist 
in the "DB2JSON" library for all tests to pass:

1. HELLO (*PGM)
2. HAMELA01 (*PGM)
3. HAMELA02 (*PGM)

This list may expand in the future to cover more use cases. Test programs will 
almost always be pulled from the DB2Sock repo: 
https://bitbucket.org/litmis/db2sock/src/master/tests/rpg/

## License
**MIT**  
This code is licensed under the MIT license. See LICENSE in project root for details.

[Back to Top](#markdown-header-table-of-contents)