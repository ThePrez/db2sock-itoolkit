/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

// Simple static class for retrieving process argument values by name
export class Args {
    // Return the given argument's value if it's been specified
    static getArgument(argumentName: string) {
        let value = '';
        for (let arg of process.argv)
        {
            if (arg.indexOf(argumentName) === 0)
            {
                value = arg.substr(argumentName.length + 1);
                break;
            }
        }

        return value;
    }
}