/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

// Expand the global namespace with a db2Url property to use throughout tests
declare module NodeJS {
    interface Global {
        db2Url: string
    }
}