/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

import { PgmParam } from './pgmparam'
import { IDataProvider } from './interfaces/IDataProvider'

export class Pgm implements IDataProvider {
    private m_Program: string;
    private m_Library: string;
    private m_Function: string | undefined;
    private m_Params: PgmParam[];

    constructor(program: string, library: string, func?: string) {
        this.m_Program = program;
        this.m_Library = library;
        this.m_Function = func;
        this.m_Params = [];
    }

    addParam(name: string, type?: string, value?: string | number) : PgmParam
    {
        let param = new PgmParam(name, type, value);
        this.m_Params.push(param);

        return param;
    }

    getParam(index: number) : PgmParam | null
    {
        if (this.m_Params.length < index)
        {
            return null;
        }

        return this.m_Params[index];
    }

    getRequestData(): object {
        let requestData: any = {};
        requestData['pgm'] = [
            { 'name': this.m_Program, 'lib': this.m_Library }
        ];

        if (this.m_Function)
        {
            requestData['pgm'][0]['func'] = this.m_Function;
        }

        if (this.m_Params.length > 0)
        {
            for (let param of this.m_Params)
            {
                requestData['pgm'].push(param.getRequestData());
            }
        }    

        return requestData;
    }

}