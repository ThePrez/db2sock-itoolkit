/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

/// <reference path="./types/global.d.ts"/>
import { Core } from '../src/core';
import { expect } from 'chai';
import 'mocha';

describe('Core constructor', () => {
    it('should return ' + global.db2Url, () => {
        const core = new Core(global.db2Url);

        const result = core.provider.remoteUrl;
        expect(result).to.equal(global.db2Url);
    });
});