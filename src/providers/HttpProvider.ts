/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

import * as request from 'request';

export class HttpProvider {
    private m_dbUser: string;
    private m_dbPass: string;
    private m_dbName: string;
    private m_remoteUrl: string;

    constructor(remoteUrl: string)
    {
        this.m_remoteUrl = remoteUrl;
    }

    send(data: object, done: (result: object) => void): void {
        var headers = {
            'Content-Type': 'application/json'
        };

        var options = {
            url: this.m_remoteUrl,
            method: 'POST',
            headers: headers,
            body: JSON.stringify(data)
        };

        request(options, (error, response, responseBody) => {
            if (!error && response.statusCode == 200) {
                done(JSON.parse(responseBody));
            }
        });
    }

    get remoteUrl(): string {
        return this.m_remoteUrl;
    }
}