/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

/// <reference path="./types/global.d.ts"/>
import { Core } from '../src/core';
import { expect } from 'chai';
import 'mocha';

describe('Qsh run', () => {
    it('should contain {"R1":"IBM"},{"R2":"QIBM"},{"R3":"QOpenSys"}', (done: () => void) => {
        const core = new Core(global.db2Url);

        let sh = core.shell('ls -1 /QOpenSys');
        core.add(sh);

        let result = core.run((result) => {
            expect(JSON.stringify(result)).to.contain('{"R1":"IBM"},{"R2":"QIBM"},{"R3":"QOpenSys"}');
            done();
        });
    });
});

describe('Qsh run2', () => {
    it('should contain "OS400', (done: () => void) => {
        const core = new Core(global.db2Url);

        let sh = core.shell('uname -a');
        core.add(sh);

        let result = core.run((result) => {
            expect(JSON.stringify(result)).to.contain('OS400');
            done();
        });
    });
});