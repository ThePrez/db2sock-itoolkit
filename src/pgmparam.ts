/* 
Copyright (c) 2018 Fresche Solutions Inc.
This code is licensed under the MIT license (see LICENSE in project root for details)
*/

import { IDataProvider } from './interfaces/IDataProvider'

export interface PgmParamData {
    m_Name: string;
    m_Type: string | undefined,
    m_Value: string | number | undefined
}

export class PgmParam implements IDataProvider, PgmParamData {
    m_Name: string;
    m_Type: string | undefined;
    m_Value: string | number | undefined;

    private m_Dim: number;
    private m_Fields: PgmParam[];

    constructor(name: string, type?: string, value?: string | number)
    {
        this.m_Name = name;
        this.m_Type = type;
        this.m_Value = value;
        this.m_Fields = [];
        this.m_Dim = 0;
    }

    addField(name: string, type: string, value: string | number): void
    {
        let field: PgmParam = new PgmParam(name, type, value);
        
        this.m_Fields.push(field);
    }

    setValue(value: string | number)
    {
        this.m_Value = value;
    }

    setDim(dim: number): void
    {
        this.m_Dim = dim;
    }

    buildFieldList(): Array<{}> {
        let fieldList: object[] = [];
        for (let field of this.m_Fields)
        {
            fieldList.push({
                'name': field.m_Name,
                'type': field.m_Type,
                'value': field.m_Value
            });
        }

        return fieldList;
    }

    getRequestData(): object {
        let returnData = {};
        if (this.m_Fields.length === 0)
        {
            return {
                's': {
                    'name': this.m_Name,
                    'type': this.m_Type,
                    'value': this.m_Value
                }
            };
        }
        else
        {
            return {
                'ds': [
                    {
                        'name': this.m_Name,
                        'dim': this.m_Dim
                    },
                    {
                        's': this.buildFieldList()
                    }
                ]
            };
        }
    }
}